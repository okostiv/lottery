#include "class_player_info.h"
#include <cstdio>
#include <cstring>

#define _cls CPlayerInfo
	// as c_player_info.cpp contains only CPlayerInfo class realization
	// there's a point in defining CPlayerInfo as _cls or whatever 
_cls::_cls(pstr const c_psName, pstr const c_psPass):
	m_uWon(0u),
	m_uLost(0u),
	m_uMoney(0u)
{
	strcpy(m_sName, c_psName);
	strcpy(m_sPass, c_psPass);
}
_cls::_cls()
	// this one creates empty (bad) instance.
{
	(*this) = _cls("","");
}
_cls::_cls(_cls const& c_Obj)
{
	(*this) = c_Obj;
}
// confirms
bool const _cls::confirm_name(pstr const c_psSource) const{
	return !strcmp(get_name(), c_psSource);	
}
bool const _cls::confirm_pass(pstr const c_psSource) const{ 
	return !strcmp(get_pass(), c_psSource);
}
// gets
pstr const _cls::get_name()  const{	return (pstr)m_sName; }
pstr const _cls::get_pass()  const{	return (pstr)m_sPass; } 
uint const _cls::get_wons()  const{	return m_uWon; }
uint const _cls::get_loses() const{	return m_uLost; }
uint const _cls::get_money() const{	return m_uMoney; }
//sets
bool const _cls::set_name(pstr const c_psNewName){
	if(strlen(c_psNewName) > (sizeof m_sName)/(sizeof str))
		return false;
	else{ 
		strcpy(m_sName, c_psNewName);
		return true;
	}
}
bool const _cls::set_pass(pstr const c_psNewPass){
	if(strlen(c_psNewPass) > (sizeof m_sPass)/(sizeof str))
		return false;
	else{ 
		strcpy(m_sPass, c_psNewPass);
		return true;
	}
}
bool const _cls::set_wons(uint const c_uNewWons){
	return m_uWon = c_uNewWons;
}
bool const _cls::set_loses(uint const c_uNewLosts){
	return m_uLost = c_uNewLosts;
}
bool const _cls::set_money(uint const c_uNewMoney){
	return m_uMoney = c_uNewMoney;
}
#undef _cls