#ifndef _COMMON_H
#define _COMMON_H

#ifndef MY_ENCHANTED_TYPES
#define MY_ENCHANTED_TYPES
typedef char str; // for arrays only 
typedef str* pstr;
typedef char int8;// to use char as signed number
typedef unsigned uint;
#endif//MY_ENCHANTED_TYPES

#ifndef NULL
#define NULL 0
#endif//NULL

#ifndef MY_STRING_SIZE_DEFINITIONS
#define MY_STRING_SIZE_DEFINITIONS
#define MICRO_STR 1<<4			// 16
#define PETTY_STR MICRO_STR<<1	// 32
#define SMALL_STR PETTY_STR<<1	// 64
#define PLAIN_STR SMALL_STR<<1	// 128
#define LARGE_STR PLAIN_STR<<1	// 256
#define GREAT_STR LARGE_STR<<1	// 512
#define GRAND_STR GREAT_STR<<1	// 1024
#define SUPER_STR GRAND_STR<<1  // 2048
#endif//MY_STRING_SIZE_DEFINITIONS

#define supercmp(x, a0, a1, a2) not(strcmp(x, a0) and strcmp(x,a1) and strcmp(x,a2))

#ifndef MY_STRING_DEFINITIONS
#define	MY_STRING_DEFINITIONS
#define STALK ">>> "
#define CTALK ""//"... "
#define ENDL  "\n"
#define TAB   "\t"
#endif//STRING_DEFINITIONS

#ifndef USER_PROFILE_MANAGER
#define USER_PROFILE_MANAGER
#define gc_psUserInfoFileName "\\$lottery.userinfo"

#endif//USER_PROFILE_MANAGER

#endif//_COMMON_H