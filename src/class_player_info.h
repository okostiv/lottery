#ifndef _PLAYER_INFO_H
#define _PLAYER_INFO_H
#include "common.h"

class CPlayerInfo{
	typedef CPlayerInfo _cls;
public:
	_cls();
		// since there's no way of creating <default user> our bad instance
		// becomes one of the possible ways of tracking the errors down
	_cls(_cls const&); 
		// copy existing instance
		// there souldn't be problems 
	_cls(pstr const, pstr const);
private:
	str m_sName[PLAIN_STR];
	str m_sPass[PLAIN_STR];
		// const-size arrays are pretty easy to write to FILE or read from one
		// so user_manager's become one point easier to make
	uint m_uWon;
	uint m_uLost;
	uint m_uMoney;
public:
	bool const confirm_name(pstr const) const;
	bool const confirm_pass(pstr const) const;

	pstr const get_name() const;
	pstr const get_pass() const;
	uint const get_wons() const;
	uint const get_loses() const;
	uint const get_money() const;

	bool const set_name(pstr const);
	bool const set_pass(pstr const);
	bool const set_wons(uint const);
	bool const set_loses(uint const);
	bool const set_money(uint const);
};
#endif