#include "common.h"
#include "class_server.h"
#include "class_game_core.h"
#include <WinSock2.h>
#include <cstdio>

int main(){
	int iResult = EXIT_FAILURE;
	do{
		HANDLE hNamedMutex = CreateMutex(NULL, NULL, "<server> has started at once.");
		if(GetLastError() == ERROR_ALREADY_EXISTS)
			break;

		puts("<server> is starting up.");
		if(!NTcp::CServer::set_up())
			break;
		else{
			puts("<server> initialization.");
			NTcp::CServer& server = NTcp::CServer::get_instance();
			if(!server.is_good_instance())
				break;
			
			puts("<server> starts working.");
			server.start_thread_of_acception_new_clients();
			server.start_thread_of_deception_new_clients();
			CGameCore::start();
			do{
				Sleep(3*1000);
				if(server.is_empty()){
					puts("<server> is empty. End session?");
					CHAR cChoice = getc(stdin);
					if(cChoice == 'y')
						break;
				}
				Sleep(100);
			}while(true);
			iResult = EXIT_SUCCESS;
		}
	}while(false);
	WSACleanup();
	return iResult;	 
}