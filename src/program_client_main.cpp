#include "common.h"

#include <cstdio>
#include <conio.h>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

DWORD WINAPI SendThread(PVOID);
DWORD WINAPI RecieveThread(PVOID);

int main(){
	do{
		WSADATA wsad;
		if(WSAStartup(MAKEWORD(2,2),&wsad)){
			puts("Winsock error - Winsock initialization failed.");
			break;
		}

		SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		if(Socket==INVALID_SOCKET){
			puts("Winsock error - Socket creation Failed.");
			break;
		}

		struct hostent *host;
		if(!(host=gethostbyname("localhost"))){
			puts("Failed to resolve hostname.");
			break;
		}

		SOCKADDR_IN SockAddr;
		SockAddr.sin_port=htons(1488);					 
		SockAddr.sin_family=AF_INET;
		SockAddr.sin_addr.s_addr=*((unsigned long*)host->h_addr);

		if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr))){
			puts("Failed to establish connection with server.");
			break;
		}else
			puts("Successfully connected to Server.");
		
		HANDLE hSendThread = CreateThread(NULL, 0, SendThread, &Socket, 0, new DWORD);
		HANDLE hRecieveThread = CreateThread(NULL, 0, RecieveThread, &Socket, 0, new DWORD);

		WaitForMultipleObjects(1, &hRecieveThread, TRUE, INFINITE);

		shutdown(Socket,SD_SEND);
		closesocket(Socket);
	}while(FALSE);
	WSACleanup();
	system("PAUSE");
	return 0;
}

DWORD WINAPI SendThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	while(TRUE){
		CHAR sSend[GRAND_STR] = {0};
		gets(sSend);
		send(Socket, sSend, min(strlen(sSend), GRAND_STR), 0);
		Sleep(100);
	}
	return 0;
}
DWORD WINAPI RecieveThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	while(TRUE){
		char buffer[GRAND_STR] = {0};
		INT iResult = recv(Socket, buffer, GRAND_STR, 0);
	    if (iResult > 0){
			printf("%s", buffer);
		}
	    else{
			if (iResult == 0)
				printf("Connection closed\n");
			else
				printf("recv failed: %d\n", WSAGetLastError());
			break;
		}
		Sleep(100);
	}
	return 0;
}