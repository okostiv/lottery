#include "thread_messaging.h"

#include <iostream>
#pragma comment(lib,"ws2_32.lib")

DWORD WINAPI SendThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	CHAR sSend[GRAND_STR];
	while(TRUE){
		std::cin.getline(sSend, GRAND_STR, '\n');
		if(! strcmp(sSend, "end already"))
			break;
		else
			send(Socket, sSend, min(strlen(sSend), GRAND_STR), 0);
		Sleep(100);
	}
	return 0;
}
DWORD WINAPI RecieveThread(PVOID pParam){
	SOCKET& Socket = *(SOCKET*)pParam;
	while(TRUE){
		char buffer[GRAND_STR] = {0};
		INT iResult = recv(Socket, buffer, GRAND_STR, 0);
	    if (iResult > 0)
			std::cout << buffer << std::endl;
	    else{
			if (iResult == 0)
				printf("Connection closed\n");
			else
				printf("recv failed: %d\n", WSAGetLastError());
			break;
			shutdown(Socket, 2);
			closesocket(Socket);
			Socket = INVALID_SOCKET;
		}
		Sleep(100);
	}
	return 0;
}