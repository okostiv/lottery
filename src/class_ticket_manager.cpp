#include "common.h"
#include "class_ticket.h"
#include "class_ticket_manager.h"

#include <cstdio>
#include <cstring>
#include <Windows.h>

#define _cls CTicketManager

_cls::_cls():
	m_data(NULL)
{}
_cls& _cls::get_instance(){
	static _cls s_tmInstance;
	return s_tmInstance;
}
FILE* _cls::open_list(pstr const mode){
	CHAR sFilePath[PLAIN_STR];
	GetCurrentDirectory(PLAIN_STR, sFilePath);
	strcat(sFilePath, sLotteryTicketsFile);
	return fopen(sFilePath, mode);
}
bool const _cls::close_list(FILE*f){
	fclose(f);
	return true;
}
bool const _cls::push_ticket(CTicket const& obj){
	FILE* f = open_list("a+b");
	fwrite(&obj, sizeof(obj), 1, f);
	close_list(f);
	return true;
}
bool const _cls::pull_ticket(CTicket* ptr){
	static CHAR sFilePath[PLAIN_STR] = {0};
	static CHAR sCopyPath[PLAIN_STR] = {0};

	if(!m_data){
		if(!strlen(sFilePath)){
			GetCurrentDirectory(PLAIN_STR, sFilePath);
			strcat(sFilePath, sLotteryTicketsFile);
			strcpy(sCopyPath, sFilePath);
			strcat(sCopyPath, "_copy");
			CopyFile(sFilePath, sCopyPath, FALSE);
		}
		m_data = fopen(sCopyPath, "rb");
		if(!m_data){
			return false;
		}
	}
	if(feof(m_data)){
		fclose(m_data);
		m_data = NULL;
		DeleteFile(sCopyPath);
		return false;
	}
	else{
		fread(ptr, sizeof(*ptr), 1, m_data);
		return true;
	}
}
