#include "common.h"
#include "class_menu.h"
#include "class_server.h"
#include "class_client.h"
#include "class_player_info.h"
#include "class_user_manager.h"
#include "class_ticket.h"
#include "class_ticket_manager.h"

#include <cstdio>
#include <cstring>

using namespace NTcp;

// welcome menu
DWORD WINAPI NMenu::Welcome(PVOID pParam){
	CClient& client = *(CClient*)pParam;
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Welcome to The Lottery." ENDL
			STALK "Make an oreder to do:" ENDL
			STALK " - _log_in_ as registered member - [#L]"	ENDL
			STALK " - _sign_up_ for membership - [#S]" ENDL
			STALK "	- view some _help_ - [#H]" ENDL
			STALK " - view some _abouts_ - [#A]" ENDL
			STALK " - disconnect and _quit_ the game - [#Q]" ENDL
			CTALK
		);
		
		CHAR sBuffer[GRAND_STR] = {0};
		client.recieve_message(sBuffer);

		CHAR cChoice;
		if (not(strcmp(sBuffer, "#L") and strcmp(sBuffer, "#l") and strcmp(sBuffer, "#log in")))
			cChoice = 'L';
		if (not(strcmp(sBuffer, "#S") and strcmp(sBuffer, "#s") and strcmp(sBuffer, "#sign up")))
			cChoice = 'S';
		if (not(strcmp(sBuffer, "#H") and strcmp(sBuffer, "#h") and strcmp(sBuffer, "#help")))
			cChoice = 'H';
		if (not(strcmp(sBuffer, "#A") and strcmp(sBuffer, "#a") and strcmp(sBuffer, "#about")))
			cChoice = 'A';
		if (not(strcmp(sBuffer, "#Q") and strcmp(sBuffer, "#q") and strcmp(sBuffer, "#quit")))
				cChoice = 'Q';

		switch(cChoice){
			case 'L':
				NMenu::LogIn(&client);
				break;
			case 'S':
				NMenu::SignUp(&client);
				break;
			case 'H':
				client.send_message(
					ENDL
					STALK "Our Lottery is a very splendid game." ENDL
					STALK "You will be able to buy some ticket's (if have some money). " ENDL
					STALK "Every 3 min there's a drawing of the win-number." ENDL
					STALK "If you guessed that number - you won. So is earned some money." ENDL
					STALK "Just type the letters in [] braces."	ENDL
				);
				break;
			case 'A':
				client.send_message(
					ENDL
					STALK "Project Lottery was created by \"Wlizlo & Me\" team." ENDL
					STALK "All rights are protected." ENDL
				);
				break;
			case 'Q':
				shutdown(client.m_connection, SD_BOTH);
				closesocket(client.m_connection);
				client.m_is_connected = false;
				return 0;
		}
		Sleep(100);
	}//while(TRUE)

}

// log in
int const WINAPI getnameLogIn(PVOID);
int const WINAPI getpassLogIn(PVOID);
int const NMenu::LogIn(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	int iResult = EXIT_FAILURE;
	do{
		if(not getnameLogIn(&client))
			break;
		if(not getpassLogIn(&client))
			break;
		iResult = EXIT_SUCCESS;
		if(NMenu::UserMenu(&client) == 10)
			return 10;
	}while(FALSE);
	return iResult;
}
int const WINAPI getnameLogIn(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	CUserManager& umanager = CUserManager::get_instance();
	HANDLE hFileMutex = CreateMutex(NULL, NULL, gc_psUserInfoFileName "_mutex");
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Type your name or [#B] to back." ENDL
			CTALK
		);
		CHAR sBuffer[PLAIN_STR] = {0};
		client.recieve_message(sBuffer);
		if(not (strcmp(sBuffer, "#B") and strcmp(sBuffer, "#b") and strcmp(sBuffer, "#back")))
			return 0;
		client.m_player = new CPlayerInfo(sBuffer, "");
		WaitForSingleObject(hFileMutex, -1);
		bool bObtain = umanager.obtain_user_profile(*(client.m_player));
		ReleaseMutex(hFileMutex);
		if(!bObtain)
		{
			client.send_message(
				ENDL
				STALK "Name wasn't found. Try one more time." ENDL
				CTALK
			);
			delete client.m_player;
		}
		else
			break;
	}
	return 1;
}
int const WINAPI getpassLogIn(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	CUserManager& umanager = CUserManager::get_instance();
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Type your password or [#B] to back." ENDL
			CTALK
		);
		CHAR sBuffer[GRAND_STR] = {0};
		client.recieve_message(sBuffer);
		if(not (strcmp(sBuffer, "#B") and strcmp(sBuffer, "#b") and strcmp(sBuffer, "#back")))
			return 0;

		if(client.m_player->confirm_pass(sBuffer)){
			client.send_message(
				ENDL
				STALK "Password confirmed. Welcome." ENDL
			);
			break;
		}
		else
			client.send_message(
				ENDL
				STALK "Wrong password." ENDL
			);
	}
	return 1;
}

// sign up
int const WINAPI getnameSignUp(PVOID);
int const WINAPI getpassSignUp(PVOID);
int const NMenu::SignUp(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	int iResult = EXIT_FAILURE;
	do{
		if(not getnameSignUp(&client))
			break;
		if(not getpassSignUp(&client))
			break;
		if(NMenu::UserMenu(&client) == 10)
			return 10;
		iResult = EXIT_SUCCESS;
	}while(FALSE);
	return iResult;
}
int const WINAPI getnameSignUp(PVOID pParam){
	CUserManager& umanager = CUserManager::get_instance();
	CClient& client = *(CClient*)(pParam);
	HANDLE hFileMutex = CreateMutex(NULL, NULL, gc_psUserInfoFileName "_mutex");
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Type your name or [#B] to back." ENDL
			CTALK
		);
		CHAR sBuffer[PLAIN_STR] = {0};
		client.recieve_message(sBuffer);
		if(not (strcmp(sBuffer, "#B") and strcmp(sBuffer, "#b") and strcmp(sBuffer, "#back")))
			return 0;
		client.m_player = new CPlayerInfo(sBuffer, "");
		WaitForSingleObject(hFileMutex, -1);
		if(not umanager.obtain_user_profile(CPlayerInfo())){
			ReleaseMutex(hFileMutex);
			client.send_message(
				ENDL
				STALK "Error. Name already exists." ENDL
			);
			delete client.m_player;
		}else
			break;
	}
	return 1;
}
int const WINAPI getpassSignUp(PVOID pParam){
	CUserManager& umanager = CUserManager::get_instance();
	CClient& client = *(CClient*)(pParam);
	HANDLE hFileMutex = CreateMutex(NULL, NULL, gc_psUserInfoFileName "_mutex");
	
	client.send_message(
		ENDL
		STALK "Type your password or [#B] to back." ENDL
		CTALK
	);
	CHAR sBuffer[PLAIN_STR] = {0};
	client.recieve_message(sBuffer);
	if(not (strcmp(sBuffer, "#B") and strcmp(sBuffer, "#b") and strcmp(sBuffer, "#back")))
		return 0;
	client.m_player->set_pass(sBuffer);
	WaitForSingleObject(hFileMutex, -1);
	umanager.create_user_profile(*(client.m_player));
	ReleaseMutex(hFileMutex);
	
	return 1;
}

// user menu
int const NMenu::UserMenu(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Play a game. (Buy a ticket.) [#P]" ENDL
			STALK "Enter chart. [#C]" ENDL
			STALK "Options. [#O]" ENDL
			STALK "Help. [#H]" ENDL
			STALK "Log out. [#L]" ENDL
			CTALK
		);
		CHAR sBuffer[GRAND_STR] = {0};
		client.recieve_message(sBuffer);
		CHAR cChoice;
		if (not(strcmp(sBuffer, "#P") and strcmp(sBuffer, "#p") and strcmp(sBuffer, "#play")))
			cChoice = 'P';
		if (not(strcmp(sBuffer, "#C") and strcmp(sBuffer, "#c") and strcmp(sBuffer, "#chart")))
			cChoice = 'C';
		if (not(strcmp(sBuffer, "#O") and strcmp(sBuffer, "#o") and strcmp(sBuffer, "#options")))
			cChoice = 'O';
		if (not(strcmp(sBuffer, "#H") and strcmp(sBuffer, "#h") and strcmp(sBuffer, "#help")))
			cChoice = 'H';
		if (not(strcmp(sBuffer, "#L") and strcmp(sBuffer, "#l") and strcmp(sBuffer, "#log out")))
			cChoice = 'L';
		switch(cChoice){
			case 'P': 
				NMenu::BuyTicket(&client);
				break;
			case 'C': 
				NMenu::EnterChart(&client);
				break;
			case 'O':
				if(NMenu::UserOptions(&client) == 10)
					return 10;
				break;
			case 'H':
				client.send_message(
					ENDL
					STALK "Buy a ticket to take part in next competition." ENDL
					STALK "Chart with other users." ENDL
				);
				break;
			case 'L':
				return EXIT_SUCCESS;
		}
	}
}

int const NMenu::BuyTicket(PVOID pParam){
	CClient& client = *(CClient*)pParam;
	CTicketManager& tmanager = CTicketManager::get_instance();
	HANDLE hFileMutex = CreateMutex(NULL, NULL, "ticket_file_mutex");
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Type 3 digits like ### or [#B] to back." ENDL
			CTALK
		);

		CHAR sBuffer[GREAT_STR] = {0};
		client.recieve_message(sBuffer);
		if(not(strcmp(sBuffer, "#B") and strcmp(sBuffer, "#b") and strcmp(sBuffer, "#back")))
			return 1;

		if(strlen(sBuffer)>3)
			client.send_message(
				ENDL
				STALK "Number is too big." ENDL
				CTALK
			);
		else{
			uint uNumber;
			sscanf(sBuffer, "%u", &uNumber);
			WaitForSingleObject(hFileMutex, -1);
			tmanager.push_ticket(CTicket(client.m_player->get_name(), uNumber));
			ReleaseMutex(hFileMutex);
			break;
		}
	}
	return 0;
}

int const NMenu::EnterChart(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	CServer& server = CServer::get_instance();
	client.m_chart_mode = true;
	client.send_message(
		ENDL
		STALK "You've entered the chart." ENDL
		ENDL
	);
	while(TRUE){
		CHAR sMessageBuffer[GREAT_STR] = {0};
		//client.send_message(CTALK);
		client.recieve_message(sMessageBuffer);
		if(not strcmp(sMessageBuffer, "#quit"))
			break;
		if(not strcmp(sMessageBuffer, "#all")){
			CHAR sBuffer[GREAT_STR] = {0};
			for(CServer::CContainer::iterator it=server.m_clients.begin(); it != server.m_clients.end(); it++){
				if( (*it)->m_chart_mode){
					strcat(sBuffer, (*it)->m_player->get_name());
					strcat(sBuffer, ENDL);
				}
			}
			client.send_message(sBuffer);
			continue;
		}
		CHAR sSendBuffer[GREAT_STR] = {0};
		sprintf(sSendBuffer, "%s: %s" ENDL, client.m_player->get_name(), sMessageBuffer);

		for(CServer::CContainer::iterator it=server.m_clients.begin(); it != server.m_clients.end(); it++){
			if( (*it)->m_chart_mode)
				if( *it != &client)
					(*it)->send_message(sSendBuffer);
		}
	}
	client.m_chart_mode = false;
	return 0;
}

int const NMenu::UserOptions(PVOID pParam){
	CClient& client = *(CClient*)(pParam);
	CUserManager& umanager = CUserManager::get_instance();
	while(TRUE){
		client.send_message(
			ENDL
			STALK "Show current info. [#I]" ENDL
			STALK "Change pass. [#P]" ENDL
			STALK "Delete user. [#D]" ENDL
			STALK "Back. [#B]" ENDL
		);
		CHAR sBuffer[GRAND_STR] = {0};
		client.recieve_message(sBuffer);
		CHAR cChoice;
		if (supercmp(sBuffer, "#I", "#i", "#info"))
			cChoice = 'I';
		if (supercmp(sBuffer, "#P", "#p", "#pass"))
			cChoice = 'P';
		if (supercmp(sBuffer, "#D", "#d", "#delete"))
			cChoice = 'D';
		if (supercmp(sBuffer, "#B", "#b", "#back"))
			cChoice = 'B';
		switch(cChoice){
			case 'I':
				sprintf(sBuffer, 
					ENDL
					STALK "username: %s" ENDL
					STALK "has money: %u" ENDL,
					client.m_player->get_name(), client.m_player->get_money()
				);
				client.send_message(sBuffer);
				break;
			case 'P':
				{
					client.send_message(
						ENDL
						STALK "Type old pass or [#B] to turn back." ENDL
						CTALK 
						);
					CHAR sGetBuf[PLAIN_STR] = {0};
					client.recieve_message(sGetBuf);
					if(supercmp(sGetBuf, "#B", "#b", "#back"))
						return 1;
					if(!client.m_player->confirm_pass(sGetBuf))
						client.send_message(
							ENDL
							STALK "Wrong pass. Try again." ENDL
							);
					else{
						client.send_message(
							ENDL
							STALK "Type new pass." ENDL
							CTALK 
						);
						CPlayerInfo pi = *(client.m_player);
						CHAR sGetBuf[PLAIN_STR] = {0};
						client.recieve_message(sGetBuf);
						pi.set_pass(sGetBuf);
						umanager.change_user_profile(*(client.m_player), pi);
						*(client.m_player) = pi;
					}
				}
				break;
			case 'D':
				umanager.perish_user_profile(*(client.m_player));
				return 10;
			case 'B':
				return 0;
		}
	}
}

