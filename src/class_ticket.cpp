#include"class_ticket.h"

#include<cstring>

#define _cls CTicket

_cls::_cls():
	m_uNumber(0u),
	m_sOwner() // <bad instance>
{}
_cls::_cls(_cls const& obj):
	m_uNumber(obj.m_uNumber)
{
	strcpy(m_sOwner, obj.m_sOwner);
}
_cls::_cls(pstr const name, uint const number):
	m_uNumber(number),
	m_sOwner() // if name is wrong ticket becomes <bad instance>
{
	if(! name)
		return;
	if(! (strlen(name) < sizeof(m_sOwner)/sizeof(char)) )
		return;
	strcpy(m_sOwner, name);
}

pstr const _cls::get_name() const{
	return (pstr)m_sOwner;
}
uint const _cls::get_number() const{
	return m_uNumber;
}

bool const _cls::set_name(pstr const name)  {
	if(!name)
		return false;
	if(strlen(name) >= sizeof(m_sOwner)/sizeof(char))
		return false;
	strcpy(m_sOwner, name);
	return true;
}
bool const _cls::set_number(uint const number){
	if(int(number/1000))
		return false;
	m_uNumber = number;
	return true;
}

bool const _cls::confirm_name(pstr const name) const{
	if(strlen(m_sOwner) != strlen(name))
		return false;
	return (!strcmp(m_sOwner, name));
}
bool const _cls::confirm_number(uint const number) const{
	return (m_uNumber == number);
}
bool const _cls::confirm_bad() const{
	return bool(strlen(m_sOwner));
}

_cls const _cls::bad_instance(){
	return _cls();
}