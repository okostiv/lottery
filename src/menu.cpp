#include "common.h"
#include "menu.h"

#include <cstdio>
#include <cstring>
#include <Windows.h>

int const (*SetTextOut)(pstr const) = NMenu::fn_iPutInConsole;
int const (*GetOrderIn)(pstr) =       NMenu::fn_iGetOfConsole;

// SetTextOut functions
int const NMenu::fn_iPutInConsole(pstr const c_psMessage){
	printf("%s", c_psMessage);
	return EXIT_SUCCESS;
}
int const NMenu::fn_iSendToClient(pstr const c_psMessage){
	// send message to current client(on the thread)
	return EXIT_SUCCESS;
}
// GetOrderIn functions
int const NMenu::fn_iGetOfConsole(pstr psBuffer)
{
	scanf("%s", psBuffer);
	return EXIT_SUCCESS;
}
int const NMenu::fn_iHaveOfClient(pstr psBuffer){
	// get message from client
	return EXIT_SUCCESS;
}
// misc
//main menu
INT CONST MainMenuParseOrder(pstr const);
INT CONST NMenu::MainMenu(){
	while(true){
		SetTextOut(S_STALK "Welcome to our little game." S_ENDL);
		SetTextOut(S_STALK "Make your order what to do." S_ENDL);
		SetTextOut(S_STALK "Log in. [L]"				 S_ENDL);
		SetTextOut(S_STALK "Sign up. [S]"				 S_ENDL);
		SetTextOut(S_STALK "View help. [H]"              S_ENDL);
		SetTextOut(S_STALK "Exit. [E]"					 S_ENDL);

		CHAR psBuffer[SMALL_STR]; // this function will be set up for quite a long time
		GetOrderIn(psBuffer);

		INT iRecievedOrder = MainMenuParseOrder(psBuffer);

		switch(iRecievedOrder){
			case (INT)'D': 
				continue;

			case (INT)'L': 
				NMenu::LogInMenu(); 
				break;
			case (INT)'S': 
				NMenu::SignUpMenu();   
				break;
			case (INT)'H': 
				NMenu::MainHelpMenu(); 
				break;
			case (INT)'E': 
				if(NMenu::ExitMainMenu())
					return EXIT_SUCCESS;
				break;
		}
	}//while
}//MainMenu
INT CONST MainMenuParseOrder(pstr const psBuffer){
	switch(psBuffer[0]){
		case 'L': case 'l': return (INT)'L';	
		case 'S': case 's':	return (INT)'S';
		case 'H': case 'h':	return (INT)'H';
		case 'E': case 'e':	return (INT)'E';
		default:			return (INT)'D';
	}		
}
// main menu parse

int const NMenu::LogInMenu()   {return 0;} // type user name and pass until matched
int const NMenu::SignUpMenu()  {return 0;}// type user name and pass until matched
int const NMenu::MainHelpMenu(){return 0;} // game reason, how to play, abouts, back 
int const NMenu::ExitMainMenu(){return 1;} //