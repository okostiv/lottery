#include "class_game_core.h"

#include "class_ticket.h"
#include "class_ticket_manager.h"
#include "class_player_info.h"
#include "class_user_manager.h"
#include "class_server.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <Windows.h>

DWORD WINAPI CoreFunction(PVOID);

#define Core CGameCore
HANDLE Core::s_hThread = NULL;
HANDLE Core::s_hTimer = NULL;
Core::TIME Core::s_tDelay = Core::TIME(0);

bool Core::start(){
	puts("<game core> starts working.");

	Core::s_hThread = CreateThread(NULL, 0, CoreFunction, NULL, HIGH_PRIORITY_CLASS, new DWORD);
	
	Core::s_hTimer = CreateWaitableTimer(NULL, FALSE, "NewLotteryDrawing");
	Core::s_tDelay  = Core::TIME(60);
	LARGE_INTEGER dt; dt.QuadPart = 1000;
	SetWaitableTimer(s_hTimer, &dt, 1000LL*Core::s_tDelay, NULL, NULL, TRUE);
	srand(time(0));
	return bool(Core::s_hThread && Core::s_hTimer);
}
bool Core::stop(){
	TerminateThread(s_hThread, 0);
	return true;
}
#define GameCoreFunctionality

bool CongratulateWinner(uint);

DWORD WINAPI CoreFunction(PVOID){
	do{
		uint uNumber = rand()%1000;
		CongratulateWinner(uNumber);
		WaitForSingleObject(Core::s_hTimer, -1);
	}while(true);
}

bool CongratulateWinner(uint uNumber){
	NTcp::CServer& server = NTcp::CServer::get_instance();
	CHAR sBuffer[PLAIN_STR] = {0};
	sprintf(sBuffer,
		ENDL
		STALK "In last competition the number %u has won." ENDL
		STALK "Don't miss next drawing." ENDL
		STALK "Good luck." ENDL, 
		uNumber
	);
	for(NTcp::CServer::CContainer::iterator it=server.m_clients.begin(); it != server.m_clients.end(); it++)
		(*it)->send_message(sBuffer);

	CTicketManager& tmanager = CTicketManager::get_instance();
	CUserManager& umanager = CUserManager::get_instance();
	while(true){
		CTicket t;
		bool cont = tmanager.pull_ticket(&t);
		if(!cont)
			break;
		if(t.confirm_number(uNumber) or t.confirm_number(777u)){
			CPlayerInfo pi(t.get_name(), "");
			umanager.obtain_user_profile(pi);
			CPlayerInfo pin(pi);
			pin.set_money(pi.get_money() + 100);
			umanager.change_user_profile(pi, pin);
		}
	}
	return true;
}									