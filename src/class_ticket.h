#include "common.h"

#ifndef _CLASS_TICKET_H_INCLUDED
#define _CLASS_TICKET_H_INCLUDED

class CTicket{
	typedef CTicket _cls;

	uint m_uNumber;
	str m_sOwner[SMALL_STR];
public:
	_cls(); // bad instance
	_cls(_cls const&);
	_cls(pstr const, uint const);

	pstr const get_name() const;
	uint const get_number() const;

	bool const set_name(pstr const);
	bool const set_number(uint const);

	bool const confirm_name(pstr const) const;
	bool const confirm_number(uint const) const;
	bool const confirm_bad() const;

	static _cls const bad_instance();
};
#endif//_CLASS_TICKET_H_INCLUDED
