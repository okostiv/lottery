#include "common.h"

#ifndef _C_USER_MANAGER
#define _C_USER_MANAGER
class CPlayerInfo;
class CUserManager{
	typedef CUserManager _cls;
	_cls();	// singleton; therefore constructor is private
	str m_sLotteryUserInfoPath[GREAT_STR]; //save file path for not to seek again
	pstr find_user_list_file();
public:
	static CUserManager& get_instance();	// class instance - the one and only obtainable object
	bool const create_user_profile(class CPlayerInfo const&) const;  // true if created new; false if existed or failed to create
	bool const perish_user_profile(class CPlayerInfo const&) const;  // true if perished; false if didnt exist 
	bool const change_user_profile(class CPlayerInfo const&, class CPlayerInfo const&) const; // true if changed; false if didnt exist
	bool const obtain_user_profile(class CPlayerInfo&) const;  // this class requires CPlayerInfo only
		// this one does confirm names for to seek but does not confirm passwords
};
#endif//_C_USER_MANAGER 

