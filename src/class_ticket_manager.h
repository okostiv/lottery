#include "common.h"

#ifndef _CLASS_TICKET_MANAGER_INCLUDED
#define _CLASS_TICKET_MANAGER_INCLUDED

class CTicketManager{
	typedef CTicketManager _cls;
	typedef struct _iobuf FILE;
private:
	FILE* m_data;
private:
	_cls();
	FILE* open_list(pstr const);
	bool const close_list(FILE*);
public:
	static _cls& get_instance();

	bool const push_ticket(class CTicket const&);
	bool const pull_ticket(class CTicket*);
	bool const clean_up();
};

/** pull_ticket usage
	
	CTicketManager& manager = CTicketManager::get_instance();
	while(1){
		CTicket t;
		bool cont = manager.pull_ticket(&t);
		if(!cont)
			break;
	}
*/

#define sLotteryTicketsFile "\\$lottery.tickets"
#endif//_CLASS_TICKET_MANAGER_INCLUDED