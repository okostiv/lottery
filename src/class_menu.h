#pragma once
#include <WinSock2.h>

namespace NMenu{
	DWORD WINAPI Welcome(PVOID);
	
	int const LogIn(PVOID);
	int const SignUp(PVOID);

	int const UserMenu(PVOID);
	
	int const BuyTicket(PVOID);
	int const EnterChart(PVOID);
	int const UserOptions(PVOID);
}