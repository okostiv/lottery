#include "common.h"

#include <cstdio>
#include <WinSock2.h>
#pragma comment(lib,"ws2_32.lib")

#include "send_recieve_thread_functins.h"

#define d_uPortAddr 1488

#include <vector>
using namespace std;

vector<SOCKET> g_socks;
vector<pair<HANDLE,HANDLE> >  g_threads;

int old_main(){
	int iResult = 1;
	do{
		if(WSAStartup(MAKEWORD(2,2), new WSADATA)){
			puts("WSAStartup failed.");
			break;
		}

		SOCKET socListener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (socListener == INVALID_SOCKET){
			puts("<listener> socket failed to initialize.");
			break;
		}

		SOCKADDR_IN saiServerInf;
		saiServerInf.sin_family = AF_INET;
		saiServerInf.sin_addr.s_addr = INADDR_ANY;
		saiServerInf.sin_port = htons(d_uPortAddr);

		if(bind(socListener,(SOCKADDR*)&saiServerInf,sizeof(saiServerInf))==SOCKET_ERROR){
			puts("<listener> socket failed to bind.");
			break;
		}

		if(listen(socListener, SOMAXCONN)){ 
			puts("listening faled.");
			break;
		}
		while(true){
			SOCKET socClient = accept(socListener, NULL, NULL);
			if(socClient == INVALID_SOCKET){
				puts("<client> socket failed to accept.");
				break;
			}
			HANDLE hSendThread = CreateThread(NULL, 0, SendThread, &socClient, 0, new DWORD);
			HANDLE hRecieveThread = CreateThread(NULL, 0, RecieveThread, &socClient, 0, new DWORD);
			g_socks.push_back(socClient);
			g_threads.push_back(make_pair(hSendThread, hRecieveThread));
		}
	}while(false);
	WSACleanup();
	system("PAUSE");
	return iResult;
};