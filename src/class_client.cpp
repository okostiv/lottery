#include "common.h"
#include "class_client.h"

#include "thread_messaging.h"
#include <cstdio>
#include "class_menu.h"
using namespace NTcp;


DWORD WINAPI RecieveInThread(PVOID pParam){
	// may become obsolete
	CClient& cl = *(CClient*)pParam;
	
	SOCKET& Socket = cl.m_connection;
	while(TRUE){
		char buffer[GRAND_STR] = {0};
		INT iResult =// recv(Socket, buffer, GRAND_STR, 0);
		cl.recieve_message(buffer);
	    if (iResult > 0)
			puts(buffer);
	    else{
			if (iResult == 0)
				printf("Connection closed\n");
			else
				printf("recv failed: %d\n", WSAGetLastError());
			cl.m_is_connected = false;
			break;
		}
		Sleep(100);
	}
	return 0;
}					 
CClient::CClient(SOCKET sock){
	m_connection = sock;
	//m_out = CreateThread(NULL, 0, SendThread, &m_connection, 0, new DWORD);
	m_in = CreateThread(NULL, 0, 
		//RecieveInThread
		NMenu::Welcome
		, this, 0, new DWORD);
	m_is_connected = true;
}
bool const CClient::is_obsolete()const{
	return !m_is_connected;
}
int const CClient::send_message(char const* c_sMessage){
	return send(m_connection, c_sMessage, strlen(c_sMessage), 0); 
}
int const CClient::recieve_message(char* sBuffer){
	return recv(m_connection, sBuffer, GRAND_STR, 0);
}