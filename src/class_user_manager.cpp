#include "class_user_manager.h"
#include "class_player_info.h"
#include <cstdio>
#include <Windows.h>

#define  _cls CUserManager
_cls::_cls()
	// create $lottery.userinfo path in local dir
{
	GetCurrentDirectory(GREAT_STR-1, m_sLotteryUserInfoPath);
	strcat(m_sLotteryUserInfoPath, gc_psUserInfoFileName);
}
_cls& _cls::get_instance(){
	static _cls s_umVar;
	return s_umVar;
}
bool const _cls::create_user_profile(CPlayerInfo const& cr_piObject) const
	// create one more object inside $lottery.userinfo
	// if there wan no one with same name
{
	str sBufferFilePath[GREAT_STR];
	sprintf(sBufferFilePath, "%s%s", m_sLotteryUserInfoPath, "_copy");
	CopyFile(m_sLotteryUserInfoPath, sBufferFilePath, FALSE);
		// create a copy of the original file

	FILE* pfCopy = fopen(sBufferFilePath, "rb");
		// a copy to read data from
	FILE* pfOrigin = fopen(m_sLotteryUserInfoPath, "wb");
		// an origin to write data to
	if(!pfCopy || !pfOrigin)
		return false;

	bool bAppend = true;
	while(!feof(pfCopy)){ 
		CPlayerInfo pi;
		fread(&pi, sizeof(pi), 1, pfCopy);
		if(pi.confirm_name(cr_piObject.get_name()))
			bAppend = false;
		fwrite(&pi, sizeof(pi), 1, pfOrigin);
	}
	if(bAppend)
		fwrite(&cr_piObject, sizeof(cr_piObject), 1, pfOrigin);

	fclose(pfCopy);
	fclose(pfOrigin);
	DeleteFile(sBufferFilePath);
		// delete useless copy
	return true;
}
bool const _cls::perish_user_profile(CPlayerInfo const& cr_piObj) const
	// delete user profile from $lotteryinfo
{
	str sBufferFilePath[GREAT_STR];
	sprintf(sBufferFilePath, "%s%s", m_sLotteryUserInfoPath, "_copy");
	CopyFile(m_sLotteryUserInfoPath, sBufferFilePath, FALSE);
		// create a copy of the original file

	FILE* pfCopy = fopen(sBufferFilePath, "rb");
		// a copy to read data from
	FILE* pfOrigin = fopen(m_sLotteryUserInfoPath, "wb");
		// an origin to write data to
	if(!pfCopy || !pfOrigin)
		return false;

	while(!feof(pfCopy)){ 
		CPlayerInfo pi;
		fread(&pi, sizeof(pi), 1, pfCopy);
		if(!pi.confirm_name(cr_piObj.get_name()))
			fwrite(&pi, sizeof(pi), 1, pfOrigin);
			//without deleting element
	}
	fclose(pfCopy);
	fclose(pfOrigin);
	DeleteFile(sBufferFilePath);
		// delete useless copy
	return true;
}
bool const _cls::change_user_profile(CPlayerInfo const& cr_piWhat, CPlayerInfo const& cr_piWith) const
{
	str sBufferFilePath[GREAT_STR];
	sprintf(sBufferFilePath, "%s%s", m_sLotteryUserInfoPath, "_copy");
	CopyFile(m_sLotteryUserInfoPath, sBufferFilePath, FALSE);
		// create a copy of the original file

	FILE* pfCopy = fopen(sBufferFilePath, "rb");
		// a copy to read data from
	FILE* pfOrigin = fopen(m_sLotteryUserInfoPath, "wb");
		// an origin to write data to
	if(!pfCopy || !pfOrigin)
		return false;

	while(!feof(pfCopy)){ 
		CPlayerInfo pi;
		fread(&pi, sizeof(pi), 1, pfCopy);
		if(!pi.confirm_name(cr_piWhat.get_name()))
			fwrite(&pi, sizeof(pi), 1, pfOrigin);
			// copy to origin
		else
			fwrite(&cr_piWith, sizeof(cr_piWith), 1, pfOrigin);
			// replacing <What> with <With>
	}
	fclose(pfCopy);
	fclose(pfOrigin);
	DeleteFile(sBufferFilePath);
		// delete useless copy
	return true;
}

bool const _cls::obtain_user_profile(CPlayerInfo& /*out*/ piObject) const
	// pi object takes name and pass
	// obtains additional data
{
	for(FILE* pf = fopen(m_sLotteryUserInfoPath, "rb"); !feof(pf);){
		CPlayerInfo pi;
		fread(&pi, sizeof(pi), 1, pf);
		if(pi.confirm_name(piObject.get_name())){
			piObject = pi;
			return true;
		}
	}
	return false;
}
#undef _cls