#include "common.h"

#ifndef _GAME_CORE_INCLUDED
#define _GAME_CORE_INCLUDED

class CGameCore{
	typedef void* HANDLE;
	typedef CGameCore _cls;
public:
	typedef uint TIME;
public:
	static HANDLE s_hThread;
	static HANDLE s_hTimer;
	static TIME s_tDelay;
public:
	static bool start();
	static bool stop();
};

#endif//_GAME_CORE_INCLUDED