#include "common.h"
#ifndef _N_MENU_INCLUDED
#define _N_MENU_INCLUDED
namespace NMenu{
		// theese are functions for sending data
	// there're two ways of performence:
	// send to client and put in console (as testing variation)
	int const fn_iSendToClient(pstr const);
	int const fn_iPutInConsole(pstr const);
	
	// these are functions for receiving data
	// there're two ways of performance
	// obtain client's order or get one from console
	int const fn_iHaveOfClient(pstr); // pstrs are buffers
	int const fn_iGetOfConsole(pstr);

	// Considering the possibility of two major actions
	// for server-client performance
	// exactly: sending and recieving data
	// we may split our functions into _in_ and _out_ ones
	// where _in_ could be client (reliease) and console(debug)
	// as well for the _out_ 
	// so that for are the two function-pointer below
	// which're gonna be used as actual functions after initialization
	// both of them are in the cpp file
//	//int const (*SetTextOut)(pstr const) = fn_iPutInConsole;
//	//int const (*GetOrderIn)(pstr) = fn_iGetOfConsole;


	// and there will be other functions to perform menues
	// MainMenu is supposed to be called in some client-server 
	// dialog thread.
	int const MainMenu();// log in, sign up, help, exit

	//Main Menu
		int const LogInMenu(); // type user name and pass until matched
		int const SignUpMenu();// type user name and pass until matched
		int const MainHelpMenu(); // game reason, how to play, abouts, back 
		int const ExitMainMenu(); // 
	//LogInMenu
	//SignUp
		//int const SeanceMenu();	// buy ticket, chat, self info, options, help, log off


}
#endif//_N_MENU_INCLUDED